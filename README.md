# Rokk3r

![alt text](/images/test.png)

## Install dependencies

Run `yarn` or `npm install`

##RUN APP (BACK & FRONT same server)

### Build front-end

Run `yarn build` or `npm build`.

### Run server  (API and FRONT)

Run `node server`. Navigate to `http://localhost:3000/`.

##RUN APP (BACK server & FRONT dev-server proxy)

### Run dev-server (FRONT)

Run `yarn start` or `npm start`. Navigate to `http://localhost:4200/`.

### Run server (API)

Run `node server`. Navigate to `http://localhost:3000/`.

## MLAB MongoDB

Connected to MLAB database ready to use CRUD services.

## Autor

Santiago García Suarez
