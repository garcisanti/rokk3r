import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Task } from './../_models/task';
import 'rxjs/add/operator/map';

@Injectable()
export class TaskService {

    // Observable array source
    private hotelsSource = new BehaviorSubject([]);

    // Observable string streams
    hotels$ = this.hotelsSource.asObservable();

    headers: HttpHeaders;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders({
          'Content-Type':  'application/json'
        });
    }

    public create(task: Task): Observable<any> {
        let params = new HttpParams().set('name', task.name);
        params = params.append('priority', '' + task.priority);
        params = params.append('dueDate', '' + task.dueDate);
        const options = {
            headers: this.headers,
            params: params
        };
        return this.http.post('api/task/create', null, options);
    }

    public getAll(): Observable<any> {
        const options = {
            headers: this.headers
        };
        return this.http.get('api/task/', options);
    }
}
