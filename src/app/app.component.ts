import { Component, OnInit } from '@angular/core';

import { TaskService } from './_services/task.service';
import { Task } from './_models/task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'app';
    creating: boolean;
    newTask: Task;
    pendingTasks: Task[];
    overDueTasks: Task[];
    allTasks: Task[];
    orderBy: string;

    constructor( private taskService: TaskService) { }

    ngOnInit() {
      const date = new Date();
      this.pendingTasks = [];
      this.overDueTasks = [];
      this.orderBy = 'none';
      this.newTask = new Task('', '', 1, date, date, date);
      this.getAll();
    }

    onSubmit() {
      this.taskService.create(this.newTask).subscribe(
          data => this.getAll(),
          err => console.log(err)
      );
    }

    getAll() {
      this.taskService.getAll().subscribe(
          data => {
              this.allTasks = data;
              this.order();
              this.separate();
          },
          err => console.log(err)
      );
    }

    separate() {
        this.pendingTasks = [];
        this.overDueTasks = [];
        const currentDate = new Date();
        for (let task of this.allTasks) {
            const taskDate = new Date(task.dueDate);
            const dif = (taskDate > currentDate)? true: false;
            if( dif ){
                this.pendingTasks.push(task);
            }
            else {
                this.overDueTasks.push(task);
            }
        }
    }

    setOrderBy(orderBy){
        this.orderBy = orderBy;
        this.order();
        this.separate();
    }

    order(){
        if(this.orderBy==='none'){
        }
        else{
            for (let i = 0; i<this.allTasks.length; i++) {
                for (let j = i+1; j<this.allTasks.length; j++) {
                    if(this.orderBy==='name'){
                        const t = this.allTasks[i];
                        const t2 = this.allTasks[j];
                        if(t.name > t2.name){
                            this.allTasks[j] = t;
                            this.allTasks[i] = t2;
                        }
                    }
                    if(this.orderBy==='dueDate'){
                        const t = this.allTasks[i];
                        const t2 = this.allTasks[j];
                        if(t.dueDate > t2.dueDate){
                            this.allTasks[j] = t;
                            this.allTasks[i] = t2;
                        }
                    }
                    if(this.orderBy==='priority'){
                        const t = this.allTasks[i];
                        const t2 = this.allTasks[j];
                        if(t.priority > t2.priority){
                            this.allTasks[j] = t;
                            this.allTasks[i] = t2;
                        }
                    }
                }
            }
        }
    }
}
