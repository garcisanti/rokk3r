export class Task {
    constructor(
        public _id: string,
        public name: string,
        public priority: number,
        public dueDate: Date,
        public createdAt: Date,
        public updatedAt: Date
    ) { }
}
