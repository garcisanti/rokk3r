var mongoose = require( 'mongoose' );

var taskSchema = new mongoose.Schema({
    name: {
      type : String,
      required: true
    },
    dueDate: {
      type : Date,
      required: true
    },
    priority: {
      type : Number,
      required: true,
      min: 1, max: 5
    },
    createdAt: {
      type : Date,
      required: true
    },
    updatedAt: {
      type : Date,
      required: true
    }
});

module.exports = mongoose.model('Task', taskSchema);
