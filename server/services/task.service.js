var express = require('express');
var taskController = require('../controllers/task.controller');

exports.create = function(req, res){
    taskController.create(req.query.name, req.query.dueDate, req.query.priority)
      .then(task => res.send(task))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.getAll = function(req, res){
    taskController.getAll()
      .then(tasks => res.send(tasks))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.destroy = function(req, res){
    taskController.destroy(req.params.id)
      .then(task => res.send(task))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.update = function(req, res){
    taskController.update(req.body)
      .then(task => res.send(task))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}
