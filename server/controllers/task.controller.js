const mongo = require('mongoose');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');
var Task = require('../models/task');

//LOCAL DATA


exports.create = (name, dueDate, priority)=>{
    var task = new Task({
        'name': name,
        'dueDate': dueDate,
        'priority': priority,
        'createdAt': new Date(),
        'updatedAt': new Date()
    })
    return task.save();
}

exports.getAll = ()=>{
    return Task.find().exec();
}

exports.update = (task)=>{

}

exports.destroy = (id)=>{

}
