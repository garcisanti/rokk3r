const express = require('express');
const router = express.Router();

const taskService = require('../services/task.service');

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

router.post('/task/create', taskService.create);
router.get('/task/', taskService.getAll);
router.get('/task/destroy/:id', taskService.destroy);
router.post('/task/update', taskService.update);

module.exports = router;
